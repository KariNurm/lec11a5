FROM node:18-alpine AS builder

WORKDIR /server

COPY ./package*.json ./
COPY ./src ./src
COPY ./tsconfig.json ./tsconfig.json

RUN npm ci
RUN npm run build

FROM node:18-alpine


WORKDIR /server
COPY --from=builder ./server/dist ./dist
COPY ./package*.json ./
RUN npm ci --production

EXPOSE 3000

CMD ["npm", "start"]